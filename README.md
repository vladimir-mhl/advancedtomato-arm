# AndreDVJ's AdvancedTomato-ARM #

Originally forked from Jacky's AdvancedTomato-ARM.

I only have the following ARM routers: Netgear R7000 and R8000. That means I'm unable to test or provide specific support to another router.

Development and maintenance here is slowing down, as I'm not able to keep this project regularly updated anymore.

PS: I build Asus targets because some people asked for them. However they only work with A1/A2 rev.
Anything else is not supported across all Tomato-ARM forks, because they have different CPU and need newer SDK.

Therefore, the same with all alternative firmwares available over the internet: Flash at your own risk.
